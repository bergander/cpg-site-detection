# CpG site detection

## Prerequisites 

input files: 

fast5-files containing raw signals of nanopore sequencing, stored in a single folder of depth 1
file with basecalled sequences of the reads (if not already done or not done with albacore/guppy: please refer 
to https://denbi-nanopore-training-course.readthedocs.io/en/stable/basecalling/basecalling.html for albacore or to https://denbi-nanopore-training-course.readthedocs.io/en/latest/basecalling/basecalling.html for instruction)

## FastQC
FastQC was utilized to determine the quality of the raw reads of the Nanopore sequencing. It provides several
analyses for high throughput sequencing data to determine problems in the data early on. [1]

## Anaconda
The package manager Anaconda was set up according to the instruction given here: https://phoenixnap.com/kb/how-to-install-anaconda-ubuntu-18-04-or-20-04. Please be aware that the location of Anaconda should provide
enough space (several gigabytes) to avoid conflicts.

Bioconda[1] is a channel for the conda package manager that provides bioinformatic software, some of which used in the following steps. To set up Bioconda and the channels Bioconda depends on, execute these commands (see https://bioconda.github.io/user/install.html#set-up-channels).

    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

Be aware that the order in which the channels are added decides the priority and shouldn't be switched up.

## Ont-fast5-api 4.0.0

As some of the tools used in following steps are not compatible with multi-read fast5 files, it is necessary to singularize the reads into single-read files. That is done by the ont fast5 API, a simple interface to fast5 files (see https://github.com/nanoporetech/ont_fast5_api). Skip this step, if the fast5 files are already single-read.
To install via conda use 

    conda install -c bioconda ont-fast5-api
    conda install -c bioconda/label/cf201901 ont-fast5-api 

To singularize the reads use

    multi_to_single_fast5 --input_path /data/multi_reads --save_path /data/single_reads --threads 6

Please note that some tools expect the fast5 folder to contain no subfolders and it is best to copy the files in all subfolders into the main folder.

# Tombo resquiggle

The signal produced by Nanopore reads is a electric current and is called a squiggle. The re-squiggle algorithm perfoms a new assignment of squiggles (therefore re-squiggle) to a given reference (e.g. genomic fasta) to diminish basecalling errors in accordance to the reference. 

The input are the fast5 files already containing base calls. If this is not the case, they have to be preprocessed to link the base call information to the corresponding signal. The main process maps the base calls to the reference and then assigns the raw signal to the reference sequence based on an expected current level model.

The preprocessing step can be done with

    tombo preprocess annotate_raw_with_fastqs --fast5-basedir path/to/fast5     
    --fastq-filenames reads.fastq --processes 4



The fast5 folder should contain all fast5 reads as direct child of the parent folder. The fastq file should contain the read sequences produced by a base caller such as guppy or albacore. It is also possible that the input is given as several fastq files. The processes flag denotes the sum of threads for the process and is dependant on the available resources.

Notice that issues with this step are rather common. As mentioned tombo does not support multi-read fast5 files. It is critical, that all files are read- and writeable and that the fastq sequence header line hast to begin with the read identifier from the fast5 files. 

The main process of the resquiggle algorithm is initiated with

    tombo resquiggle path/to/fast5s/ reference.fasta --processes 4 --num-most-common-errors 5

Num most common errors lists the 5 most common reasons, why reads were not successfully processed. It is recommended to use this flag, as it simplifies the debugging process. 
Note, that the placeholder nucleotide 'N' can lead to issues in the mapping process. To avoid many unprocessed reads, it is beneficial to infer the correct nucleotide and replace the Ns. If not possible, it may be helpful to split the reference at the start and at the end of stretches of Ns and work with the resulting contigs of known sequences. 

If a former resquiggle process was not terminated correctly, it may be necessary to set the flags
    --overwrite
    --ignore-read-locks 


# DeepSignal

DeepSignal is a deep learning method to detect DNA methylation states from Nanopore sequencing reads.[DeepSignal] As input the resquiggled fast5 files, a genome reference and a pre-trained model for prediction, which can be downloaded (https://drive.google.com/drive/folders/1zkK8Q1gyfviWWnXUBMcIwEDw3SocJg7P) or be trained individually. Detailed instruction can be found at https://github.com/bioinfomaticsCSU/deepsignal.
To call DeepSignal, the following command can be used. 

    deepsignal call_mods --input_path path/to/fast5s/ 
    --model_path path/to/model.ckpt 
    --result_file fast5s.call_mods.tsv 
    --reference_path path/to/reference.fasta

The result is a tab-delimited file with the methylation that were identified through the chosen model. For the current DeepSignal version the CpG model trained using HX1 R9.4 1D reads can be used.

[1] https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
[2]Grüning, Björn, Ryan Dale, Andreas Sjödin, Brad A. Chapman, Jillian Rowe, Christopher H. Tomkins-Tinch, Renan Valieris, the Bioconda Team, and Johannes Köster. 2018. “Bioconda: Sustainable and Comprehensive Software Distribution for the Life Sciences”. Nature Methods, 2018 doi:10.1038/s41592-018-0046-7.
[DeepSignal]Bioinformatics, Volume 35, Issue 22, 15 November 2019, Pages 4586–4595, https://doi.org/10.1093/bioinformatics/btz276
